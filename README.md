# Developer Workstation Configuration

Automation, scripts, and documentation for the management of my development workstation.

Configuration files themselves are managed in [this dotfiles repository](https://gitlab.com/jamestarka/dotfiles).

Based on:
- [Jeff Geerling's Mac development playbook](https://github.com/geerlingguy/mac-dev-playbook)

## Status

The contents of this repository exist primarily for my own development purposes. It is available publically as a resource for anyone wants to structure their own setup after mine or see how this could be done.

## Requirements

- Ubuntu
    - Minimum version TBD
    - May also work on other Debian-based distributions
- Python
    - Minimum version TBD
- Ansible
    - Minimum verison TBD

## Installation

Clone this repository to your local drive.

    $ git clone git@gitlab.com:jamestarka/developer-workstation.git

From the cloned directory, install the required Ansible dependencies.

    $ ansible-galaxy install -r requirements.yml

## Usage

The [default variables file](./defaults/main.yml) includes explanations for the tasks and variables that can be configured within this role. See the section below on overriding the default configuration if needed. This will allow you to vary your configuration on different machines while still using the same basic setup.

Run the main playbook within this directory.

    $ ansible-playbook main.yml -i inventory --ask-become-pass

You will be prompted for your password to continue.

### Overriding Defaults

You can override variables as needed by editing the file ["main.yml" in the "vars" directory](./vars/main.yml).

### Configuring New Software

This section describes how to add configuration for software that does not already have a configuration defined within this repository.

1. Create a new folder for the new software within the ``tasks`` folder.
    * The folder name should be named appropriately for the software to be configured, for example by using the software or tool's name.
    * For example, the software called ``Foo`` would get a folder called ``foo``.
    * Use dashes to separate software names that include spaces.
2. Create a new file ``main.yml`` within the new folder.
3. Update the ``main.yml`` to include the Ansible tasks to run.
4. Add a line ``configure_{{software-name}}=true`` to the file [defaults/main.yml](./defaults/main.yml) in the same section as the other similar lines.
    * Replace ``{{software-name}}`` with the name of the software to configure. The name should match the name of the associated tasks folder.
    * Try to keep the ``configure_*`` entries in alphabetical order by configured software name.
    * For example, if there is a folder called ``tasks/foo``, then there should be a line ``configure_foo=true``.
5. (optional) If the configuration tasks utilize any variables, add a section to the [defaults/main.yml](./defaults/main.yml) to include default values for those variables.
6. Add a section to the [main.yml](./main.yml) file to run the configuration for the software when the ``configure_{{software-name}}`` variable is true.
    * See the existing entries for this should look like.
    * If possible, try to order the entries alphabetically by configured software name.

## Testing

TODO: build out this section, consider using Vagrant?

## Author Information

James Tarka