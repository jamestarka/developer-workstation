# Tasks

Ansible tasks related to workstation configuration. Tasks should be broken down by the application being configured or in the case of non-application-specific items, by some kind of logical grouping.

Within each category, the "main" playbook will always be executed (unless it is setup to not be configured). Within the "main.yml" playbook, you can choose to include other playbooks within that directory (perhaps based on other variables).

To add a new category with name "example":
- Create a directory within "tasks" called "example".
- Add a variable "configure_example" to defaults/main.yml.
- Add logic to "main.yml" to include "tasks/example/main.yml" when "configure_example" is set.